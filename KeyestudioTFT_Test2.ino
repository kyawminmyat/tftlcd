// BMP-loading example specifically for the TFTLCD breakout board.
// If using the Arduino shield, use the tftbmp_shield.pde sketch instead!
// If using an Arduino Mega make sure to use its hardware SPI pins, OR make
// sure the SD library is configured for 'soft' SPI in the file Sd2Card.h.

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library
//#include <MCUFRIEND_kbv.h>   // Hardware-specific library
//MCUFRIEND_kbv tft;
#include <SD.h>
#include <SPI.h>

//#include <Fonts/FreeSans9pt7b.h>
//#include <Fonts/FreeSans12pt7b.h>
//#include <Fonts/FreeSerif12pt7b.h>
//#include <FreeDefaultFonts.h>
//#include <Fonts/FreeMono9pt7b.h>

// The control pins for the LCD can be assigned to any digital or
// analog pins...but we'll use the analog pins as this allows us to
// double up the pins with the touch screen (see the TFT paint example).
#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0

// our TFT wiring
Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, A4);

// Assign human-readable names to some common 16-bit color values:
#define BLACK     0x0000
#define NAVY      0x000F
#define DARKGREEN 0x03E0
#define DARKCYAN  0x03EF
#define MAROON    0x7800
#define PURPLE    0x780F
#define OLIVE     0x7BE0
#define LIGHTGREY 0xC618
#define DARKGREY  0x7BEF
#define BLUE      0x001F
#define GREEN     0x07E0
#define CYAN      0x07FF
#define RED       0xF800
#define MAGENTA   0xF81F
#define YELLOW    0xFFE0
#define WHITE     0xFFFF
#define ORANGE    0xFD20
#define GREENYELLOW 0xAFE5
#define PINK      0xF81F

// http://www.barth-dev.de/online/rgb565-color-picker/
#define LTBLUE    0xB6DF
#define LTTEAL    0xBF5F
#define LTGREEN   0xBFF7
#define LTCYAN    0xC7FF
#define LTRED     0xFD34
#define LTMAGENTA  0xFD5F
#define LTYELLOW   0xFFF8
#define LTORANGE   0xFE73
#define LTPINK     0xFDDF
#define LTPURPLE   0xCCFF
#define LTGREY     0xE71C

#define BLUE       0x001F
#define TEAL       0x0438
#define GREEN      0x07E0
#define CYAN       0x07FF
#define RED        0xF800
#define MAGENTA    0xF81F
#define YELLOW     0xFFE0
#define ORANGE     0xFD20
#define PINK       0xF81F
#define PURPLE     0x801F
#define GREY       0xC618
#define WHITE      0xFFFF
#define BLACK      0x0000

#define DKBLUE     0x000D
#define DKTEAL     0x020C
#define DKGREEN    0x03E0
#define DKCYAN     0x03EF
#define DKRED      0x6000
#define DKMAGENTA  0x8008
#define DKYELLOW   0x8400
#define DKORANGE   0x8200
#define DKPINK     0x9009
#define DKPURPLE   0x4010
#define DKGREY     0x4A49

// Meter colour schemes
#define RED2RED 0
#define GREEN2GREEN 1
#define BLUE2BLUE 2
#define BLUE2RED 3
#define GREEN2RED 4
#define RED2GREEN 5

#define MINPRESSURE 10
#define MAXPRESSURE 1000

// When using the BREAKOUT BOARD only, use these 8 data lines to the LCD:
// For the Arduino Uno, Duemilanove, Diecimila, etc.:
//   D0 connects to digital pin 8  (Notice these are
//   D1 connects to digital pin 9   NOT in order!)
//   D2 connects to digital pin 2
//   D3 connects to digital pin 3
//   D4 connects to digital pin 4
//   D5 connects to digital pin 5
//   D6 connects to digital pin 6
//   D7 connects to digital pin 7
// For the Arduino Mega, use digital pins 22 through 29
// (on the 2-row header at the end of the board).

// For Arduino Uno/Duemilanove, etc
//  connect the SD card with DI going to pin 11, DO going to pin 12 and SCK going to pin 13 (standard)
//  Then pin 10 goes to CS (or whatever you have set up)
#define SD_CS 10     // Set the chip select line to whatever you use (10 doesnt conflict with the library)

// In the SD card, place 24 bit color BMP files (be sure they are 24-bit!)
// There are examples in the sketch folder


int col[8];
int reading = 100.12; // Value to be displayed
float uab, ubc, uac, ull, i1, i2, i3, i0, freq, temp, humid, energy;
double a1, b1, c1, d1, r2, r1, vo, tempC, tempF, tempK;

double x, y;
// this is the only external variable used by the graph
// it's a flat to draw the coordinate system only on the first pass
boolean display1 = true;
boolean display2 = true;
boolean display3 = true;
boolean display4 = true;
boolean display5 = true;
boolean display6 = true;
boolean display7 = true;
boolean display8 = true;
boolean display9 = true;
double ox , oy ;
uint32_t cur = 0, prv = 0;
unsigned int color;
uint8_t count = 0;

void setup()
{
  a1 = 3.354016E-03 ;
  b1 = 2.569850E-04 ;
  c1 = 2.620131E-06 ;
  d1 = 6.383091E-08 ;
  Serial.begin(9600);

  tft.reset();
  tft.begin(0x9325);
  tft.fillScreen(BLACK);
  tft.setRotation(3);

  Serial.print(F("Initializing SD card..."));
  if (!SD.begin(SD_CS)) 
  {
    Serial.println(F("failed!"));
    return;
  }
  Serial.println(F("OK!"));

  bmpDraw("sub1.bmp", 3, 0);  //115,0
  
  tft.drawFastHLine(0,102,320,RED); //x,y,length,color
  
  tft.setTextColor(ORANGE); //setTextColor(uint16_t t)
  tft.setTextSize(1); //setTextSize(uint8_t s)
  tft.setCursor(115,10); //setCursor(int16_t x, int16_t y)
  tft.print("Powered by Bits Manager Co,Ltd"); // Prints the string

  tft.setTextColor(YELLOW); //setTextColor(uint16_t t)
  tft.setTextSize(2); //setTextSize(uint8_t s)
  tft.setCursor(170,35); //setCursor(int16_t x, int16_t y)
  tft.print("Energy"); // Prints the string
  tft.fillRoundRect (105, 65, 214, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (105, 65, 214, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame

  tft.setTextColor(YELLOW); //setTextColor(uint16_t t)
  tft.setTextSize(1); //setTextSize(uint8_t s)
  tft.setCursor(3,117); //setCursor(int16_t x, int16_t y)
  tft.print("U(ab):"); // Prints the string
  tft.setCursor(3,152); //setCursor(int16_t x, int16_t y)
  tft.print("U(bc):"); // Prints the string
  tft.setCursor(3,185); //setCursor(int16_t x, int16_t y)
  tft.print("U(ac):"); // Prints the string
  tft.setCursor(3,219); //setCursor(int16_t x, int16_t y)
  tft.print("U(ll):"); // Prints the string

  tft.setCursor(133,117); //setCursor(int16_t x, int16_t y)
  tft.print("I1:"); // Prints the string
  tft.setCursor(133,152); //setCursor(int16_t x, int16_t y)
  tft.print("I2:"); // Prints the string
  tft.setCursor(133,185); //setCursor(int16_t x, int16_t y)
  tft.print("I3:"); // Prints the string
  tft.setCursor(133,219); //setCursor(int16_t x, int16_t y)
  tft.print("I0:"); // Prints the string

  tft.setCursor(225,117); //setCursor(int16_t x, int16_t y)
  tft.print("Freq:"); // Prints the string
  tft.setCursor(225,152); //setCursor(int16_t x, int16_t y)
  tft.print("Temp:"); // Prints the string
  tft.setCursor(225,185); //setCursor(int16_t x, int16_t y)
  tft.print("Humid:"); // Prints the string
/****************First Column**************************/
  tft.fillRoundRect (40, 106, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (40, 106, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
  tft.fillRoundRect (40, 139, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (40, 139, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
  tft.fillRoundRect (40, 172, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (40, 172, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
  tft.fillRoundRect (40, 205, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (40, 205, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
/****************First Column**************************/
/****************Second Column**************************/
  tft.fillRoundRect (150, 106, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (150, 106, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
  tft.fillRoundRect (150, 139, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (150, 139, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
  tft.fillRoundRect (150, 172, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (150, 172, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
  tft.fillRoundRect (150, 205, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (150, 205, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
/****************Second Column**************************/
/****************Third Column**************************/
  tft.fillRoundRect (260, 106, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (260, 106, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
  tft.fillRoundRect (260, 139, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (260, 139, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
  tft.fillRoundRect (260, 172, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
  tft.drawRoundRect (260, 172, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
//  tft.fillRoundRect (150, 205, 60, 30, 10, WHITE); //(x,y,w,h,r,t) Draws filled rounded rectangle
//  tft.drawRoundRect (150, 205, 60, 30, 10, RED); // Draws rounded rectangle without a fill, so the overall appearance of the button looks like it has a frame
/****************Third Column**************************/
  
}

void loop()
{
  uab   = 123.12;
  ubc   = 123.12;
  uac   = 123.12;
  ull   = 123.12;
  i1    = 10.12;
  i2    = 10.12;
  i3    = 10.12;
  i0    = 10.12;
  freq  = 50.12;
  temp  = 30.12;
  humid = 50.12;
  energy = 12345.12;
/****************First Column**************************/
  //  tft.write(c); //write(uint8_t c)
  tft.setTextColor(RED);
  tft.setTextSize(1); 
  tft.setCursor(47,116); 
  tft.print(Format(uab,1,2));tft.print(" V"); //Format(val,dec,dig)
  tft.setCursor(47,148); 
  tft.print(Format(ubc,1,2));tft.print(" V");
  tft.setCursor(47,182); 
  tft.print(Format(uac,1,2));tft.print(" V");
  tft.setCursor(47,217); 
  tft.print(Format(ull,1,2));tft.print(" V"); 
/****************First Column**************************/
/****************Second Column**************************/
  tft.setCursor(160,116); 
  tft.print(Format(i1,1,2));tft.print(" A"); 
  tft.setCursor(160,148); 
  tft.print(Format(i2,1,2));tft.print(" A"); 
  tft.setCursor(160,182); 
  tft.print(Format(i3,1,2));tft.print(" A"); 
  tft.setCursor(160,217); 
  tft.print(Format(i0,1,2));tft.print(" A"); 
/****************Second Column**************************/
/****************Third Column***************************/
  tft.setCursor(270,116); 
  tft.print(Format(freq,1,2));tft.print(" Hz"); 
  tft.setCursor(270,148); 
  tft.print(Format(temp,1,2));tft.print(" C");
  tft.setCursor(270,182); 
  tft.print(Format(humid,1,2));tft.print(" %");
//  tft.setCursor(270,217); 
//  tft.print(Format(fval,1,2)); 
/****************Third Column***************************/
/****************Energy***************************/
  tft.setTextColor(RED);
  tft.setTextSize(2); 
  tft.setCursor(110,72); 
  tft.print(Format(energy,1,2));
  tft.setTextSize(2);
  tft.setCursor(265,72);
  tft.print("kW.h"); //Format(val,dec,dig)
/****************Energy***************************/
  cur = millis();
  if(cur - prv > 1000)
  {
    for (x = 0; x <= 6.3; x += .1) 
    {
      y = sin(x);
      Graph(x, y, 237, 236, 65, 30, 0, 6.5, 1, -1, 1, .25, "Sin Function", "x", "sin(x)", DKBLUE, RED, BLACK, WHITE, BLACK, display1);
    }
    display1 = true;
    count++;
    if(count > 9)count = 0;
    switch(count)
    {
      case 0: color = YELLOW;break;      
      case 1: color = RED;break;
      case 2: color = BLUE;break;
      case 3: color = ORANGE;break;
      case 4: color = CYAN;break;
      case 5: color = PURPLE;break;
      case 6: color = MAGENTA;break;
      case 7: color = GREEN;break;
      case 8: color = TEAL;break; 
      default: color = WHITE;        
    }
    for (x = 0; x <= 6.3; x += .1) 
    {
      y = sin(x);
      Graph(x, y, 237, 236, 65, 30, 0, 6.5, 1, -1, 1, .25, "Sin Function", "x", "sin(x)", DKBLUE, RED, color, WHITE, BLACK, display1);
      delay(20);
    }
    display1 = true;
    prv = millis();
  }
  
}

// This function opens a Windows Bitmap (BMP) file and
// displays it at the given coordinates.  It's sped up
// by reading many pixels worth of data at a time
// (rather than pixel by pixel).  Increasing the buffer
// size takes more of the Arduino's precious RAM but
// makes loading a little faster.  20 pixels seems a
// good balance.

#define BUFFPIXEL 20

void bmpDraw(char *filename, int x, int y) {

  File     bmpFile;
  int      bmpWidth, bmpHeight;   // W+H in pixels
  uint8_t  bmpDepth;              // Bit depth (currently must be 24)
  uint32_t bmpImageoffset;        // Start of image data in file
  uint32_t rowSize;               // Not always = bmpWidth; may have padding
  uint8_t  sdbuffer[3*BUFFPIXEL]; // pixel in buffer (R+G+B per pixel)
  uint16_t lcdbuffer[BUFFPIXEL];  // pixel out buffer (16-bit per pixel)
  uint8_t  buffidx = sizeof(sdbuffer); // Current position in sdbuffer
  boolean  goodBmp = false;       // Set to true on valid header parse
  boolean  flip    = true;        // BMP is stored bottom-to-top
  int      w, h, row, col;
  uint8_t  r, g, b;
  uint32_t pos = 0, startTime = millis();
  uint8_t  lcdidx = 0;
  boolean  first = true;

  if((x >= tft.width()) || (y >= tft.height())) return;

  Serial.println();
  Serial.print(F("Loading image '"));
  Serial.print(filename);
  Serial.println('\'');
  // Open requested file on SD card
  if ((bmpFile = SD.open(filename)) == NULL) {
    Serial.println(F("File not found"));
    return;
  }

  // Parse BMP header
  if(read16(bmpFile) == 0x4D42) { // BMP signature
    Serial.println(F("File size: ")); Serial.println(read32(bmpFile));
    (void)read32(bmpFile); // Read & ignore creator bytes
    bmpImageoffset = read32(bmpFile); // Start of image data
    Serial.print(F("Image Offset: ")); Serial.println(bmpImageoffset, DEC);
    // Read DIB header
    Serial.print(F("Header size: ")); Serial.println(read32(bmpFile));
    bmpWidth  = read32(bmpFile);
    bmpHeight = read32(bmpFile);
    if(read16(bmpFile) == 1) { // # planes -- must be '1'
      bmpDepth = read16(bmpFile); // bits per pixel
      Serial.print(F("Bit Depth: ")); Serial.println(bmpDepth);
      if((bmpDepth == 24) && (read32(bmpFile) == 0)) { // 0 = uncompressed

        goodBmp = true; // Supported BMP format -- proceed!
        Serial.print(F("Image size: "));
        Serial.print(bmpWidth);
        Serial.print('x');
        Serial.println(bmpHeight);

        // BMP rows are padded (if needed) to 4-byte boundary
        rowSize = (bmpWidth * 3 + 3) & ~3;

        // If bmpHeight is negative, image is in top-down order.
        // This is not canon but has been observed in the wild.
        if(bmpHeight < 0) {
          bmpHeight = -bmpHeight;
          flip      = false;
        }

        // Crop area to be loaded
        w = bmpWidth;
        h = bmpHeight;
        if((x+w-1) >= tft.width())  w = tft.width()  - x;
        if((y+h-1) >= tft.height()) h = tft.height() - y;

        // Set TFT address window to clipped image bounds
        tft.setAddrWindow(x, y, x+w-1, y+h-1);

        for (row=0; row<h; row++) { // For each scanline...
          // Seek to start of scan line.  It might seem labor-
          // intensive to be doing this on every line, but this
          // method covers a lot of gritty details like cropping
          // and scanline padding.  Also, the seek only takes
          // place if the file position actually needs to change
          // (avoids a lot of cluster math in SD library).
          if(flip) // Bitmap is stored bottom-to-top order (normal BMP)
            pos = bmpImageoffset + (bmpHeight - 1 - row) * rowSize;
          else     // Bitmap is stored top-to-bottom
            pos = bmpImageoffset + row * rowSize;
          if(bmpFile.position() != pos) { // Need seek?
            bmpFile.seek(pos);
            buffidx = sizeof(sdbuffer); // Force buffer reload
          }

          for (col=0; col<w; col++) { // For each column...
            // Time to read more pixel data?
            if (buffidx >= sizeof(sdbuffer)) { // Indeed
              // Push LCD buffer to the display first
              if(lcdidx > 0) {
                tft.pushColors(lcdbuffer, lcdidx, first);
                lcdidx = 0;
                first  = false;
              }
              bmpFile.read(sdbuffer, sizeof(sdbuffer));
              buffidx = 0; // Set index to beginning
            }

            // Convert pixel from BMP to TFT format
            b = sdbuffer[buffidx++];
            g = sdbuffer[buffidx++];
            r = sdbuffer[buffidx++];
            lcdbuffer[lcdidx++] = tft.color565(r,g,b);
          } // end pixel
        } // end scanline
        // Write any remaining data to LCD
        if(lcdidx > 0) {
          tft.pushColors(lcdbuffer, lcdidx, first);
        } 
        Serial.print(F("Loaded in "));
        Serial.print(millis() - startTime);
        Serial.println(" ms");
      } // end goodBmp
    }
  }

  bmpFile.close();
  if(!goodBmp) Serial.println(F("BMP format not recognized."));
}

// These read 16- and 32-bit types from the SD card file.
// BMP data is stored little-endian, Arduino is little-endian too.
// May need to reverse subscript order if porting elsewhere.

uint16_t read16(File f) {
  uint16_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read(); // MSB
  return result;
}

uint32_t read32(File f) {
  uint32_t result;
  ((uint8_t *)&result)[0] = f.read(); // LSB
  ((uint8_t *)&result)[1] = f.read();
  ((uint8_t *)&result)[2] = f.read();
  ((uint8_t *)&result)[3] = f.read(); // MSB
  return result;
}

/*
  function to draw a cartesian coordinate system and plot whatever data you want
  just pass x and y and the graph will be drawn
  huge arguement list
  &d name of your display object
  x = x data point
  y = y datapont
  gx = x graph location (lower left)
  gy = y graph location (lower left)
  w = width of graph
  h = height of graph
  xlo = lower bound of x axis
  xhi = upper bound of x asis
  xinc = division of x axis (distance not count)
  ylo = lower bound of y axis
  yhi = upper bound of y asis
  yinc = division of y axis (distance not count)
  title = title of graph
  xlabel = x asis label
  ylabel = y asis label
  gcolor = graph line colors
  acolor = axi ine colors
  pcolor = color of your plotted data
  tcolor = text color
  bcolor = background color
  &redraw = flag to redraw graph on fist call only
*/


void Graph(double x, double y, double gx, double gy, double w, double h, double xlo, double xhi, double xinc, double ylo, double yhi, double yinc, String title, String xlabel, String ylabel, unsigned int gcolor, unsigned int acolor, unsigned int pcolor, unsigned int tcolor, unsigned int bcolor, boolean &redraw) {

  double ydiv, xdiv;
  // initialize old x and old y in order to draw the first point of the graph
  // but save the transformed value
  // note my transform funcition is the same as the map function, except the map uses long and we need doubles
  //static double ox = (x - xlo) * ( w) / (xhi - xlo) + gx;
  //static double oy = (y - ylo) * (gy - h - gy) / (yhi - ylo) + gy;
  double i;
  double temp;
  int rot, newrot;

  if (redraw == true) {

    redraw = false;
    ox = (x - xlo) * ( w) / (xhi - xlo) + gx;
    oy = (y - ylo) * (gy - h - gy) / (yhi - ylo) + gy;
  }

  //graph drawn now plot the data
  // the entire plotting code are these few lines...
  // recall that ox and oy are initialized as static above
  x =  (x - xlo) * ( w) / (xhi - xlo) + gx;
  y =  (y - ylo) * (gy - h - gy) / (yhi - ylo) + gy;
  tft.drawLine(ox, oy, x, y, pcolor);
  tft.drawLine(ox, oy + 1, x, y + 1, pcolor);
  tft.drawLine(ox, oy - 1, x, y - 1, pcolor);
  ox = x;
  oy = y;

}

// #########################################################################
// Return a value in range -1 to +1 for a given phase angle in degrees
// #########################################################################
float sineWave(int phase) {
  return sin(phase * 0.0174532925);
}

String Format(double val, int dec, int dig ) {
  int addpad = 0;
  char sbuf[20];
  String condata = (dtostrf(val, dec, dig, sbuf));


  int slen = condata.length();
  for ( addpad = 1; addpad <= dec + dig - slen; addpad++) {
    condata = " " + condata;
  }
  return (condata);

}
